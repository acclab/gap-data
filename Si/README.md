Si GAP with accurate tabulated repulsive potential added in [1]. Original GAP is from [2].

[1] A. Hamedani. J. Byggmästar, F. Djurabekova, G. Alahyarizadeh, R. Ghaderi, A. Minuchehr, and K. Nordlund, Insights into the primary radiation damage of silicon by a machine learning interatomic potential, Materials Research Letters, 8, 10, 364-372 (2020), <https://doi.org/10.1080/21663831.2020.1771451>

[2] A. P. Bartók, J. Kermode, N. Bernstein, and G. Csányi, Machine Learning a General-Purpose Interatomic Potential for Silicon, Phys. Rev. X 8, 041048 (2018), <https://doi.org/10.1103/PhysRevX.8.041048>
